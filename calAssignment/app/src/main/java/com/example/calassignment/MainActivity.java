package com.example.calassignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button b1,b2,b3,b4,b5,b6,b7,b8,b9,b0,badd,bsub,bdiv,bmul,bdot,beql,btnClear,btnLB,btnRB,btnE,btnPie,btnSin,btnDeg;
    TextView answer,history;
    double v1 =0;
    double v2 =0;
    double other =0;
    boolean add,sub,div,mul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // To remove the default action Bar
        getSupportActionBar().hide();
        //initialize buttons
        b1=(Button) findViewById(R.id.btn_1);
        b2=(Button) findViewById(R.id.btn_2);
        b3=(Button) findViewById(R.id.btn_3);
        b4=(Button) findViewById(R.id.btn_4);
        b5=(Button) findViewById(R.id.btn_5);
        b6=(Button) findViewById(R.id.btn_6);
        b7=(Button) findViewById(R.id.btn_7);
        b8=(Button) findViewById(R.id.btn_8);
        b9=(Button) findViewById(R.id.btn_9);
        b0=(Button) findViewById(R.id.btn_0);
        badd=(Button) findViewById(R.id.btn_plus);
        bsub=(Button) findViewById(R.id.btn_minus);
        bmul=(Button) findViewById(R.id.btn_multiply);
        bdiv=(Button) findViewById(R.id.btn_divide);
        bdot=(Button) findViewById(R.id.btn_dot);
        beql=(Button) findViewById(R.id.btn_eql);
        btnClear=(Button) findViewById(R.id.btn_c);
        btnLB=(Button) findViewById(R.id.btn_leftBracket);
        btnRB=(Button) findViewById(R.id.btn_rightBracket);
        btnE=(Button) findViewById(R.id.btn_e);
        btnPie=(Button) findViewById(R.id.btn_pie);
        btnSin=(Button) findViewById(R.id.btn_sin);
        btnDeg=(Button) findViewById(R.id.btn_deg);
        //Initialize textViews
        answer =(TextView)findViewById(R.id.Answer) ;
        history=(TextView)findViewById(R.id.History) ;

        history.setVisibility(TextView.VISIBLE);
        answer.setVisibility(TextView.VISIBLE);

        //Action on button clicks
        b0.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"0");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"0");
            }
        });
        b1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"1");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"1");
            }
        });
        b2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"2");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"2");
            }
        });
        b3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"3");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"3");
            }
        });
        b4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"4");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"4");
            }
        });
        b5.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"5");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"5");
            }
        });
        b6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"6");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"6");
            }
        });
        b7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"7");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"7");
            }
        });
        b8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"8");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"8");
            }
        });
        b9.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"9");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"9");
            }
        });
        bdot.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+".");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+".");
            }
        });
        btnSin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                try{
                    if(answer.getText()!= null) {
                        other = Math.sin(Math.toRadians(Double.parseDouble(answer.getText() + "")));
                        history.setText("sin(" + answer.getText() + ")");
                        answer.setHint("sin(" + answer.getText() + ")");
                        answer.setText(other + "");
                    }else{
                        answer.setText(answer.getText()+"0.0");
                        history.setText(history.getText()+"0.0");
                    }
                }catch(Exception e){
                    answer.setText(answer.getText()+"0.0");
                    history.setText(history.getText()+"0.0");
                }
            }
        });
        btnDeg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                try{
                    if(answer.getText()!= null) {
                        other = Math.toDegrees(Double.parseDouble(answer.getText() + ""));
                        history.setText("deg(" + answer.getText() + ") => rad");
                        answer.setHint("deg(" + answer.getText() + ")");
                        answer.setText(other + "");
                    }else{
                        answer.setText(answer.getText()+"0.0");
                        history.setText(history.getText()+"0.0");
                    }
                }catch(Exception e){
                    answer.setText(answer.getText()+"0.0");
                    history.setText(history.getText()+"0.0");
                }
            }
        });
        btnE.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                try{
                    if(answer.getText()!= null) {
                        other = Math.exp(Double.parseDouble(answer.getText() + ""));
                        history.setText("exp(" + answer.getText() + ")");
                        answer.setHint("exp(" + answer.getText() + ")");
                        answer.setText(other + "");
                    }else{
                        answer.setText(answer.getText()+"2.718281828459045");
                        answer.setVisibility(TextView.INVISIBLE);
                        history.setVisibility(TextView.VISIBLE);
                        history.setText(history.getText()+"2.718281828459045");
                    }
                }catch(Exception e){
                    answer.setText(answer.getText()+"2.718281828459045");
                    answer.setVisibility(TextView.INVISIBLE);
                    history.setVisibility(TextView.VISIBLE);
                    history.setText(history.getText()+"2.718281828459045");
                }
            }
        });
        btnPie.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(answer.getText()+"3.142857142857143");
                answer.setVisibility(TextView.INVISIBLE);
                history.setVisibility(TextView.VISIBLE);
                history.setText(history.getText()+"3.142857142857143");
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                answer.setText(null);
                history.setText(null);
                answer.setHint("0");
                history.setVisibility(TextView.VISIBLE);
                answer.setVisibility(TextView.VISIBLE);
            }
        });
        btnLB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                history.setText(history.getText()+"*(");
                try{
                    v1= Double.parseDouble(answer.getText()+"");
                    mul=true;
                    answer.setHint(answer.getText());
                    answer.setText(null);
                }catch(Exception e){}
            }
        });
        btnRB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                history.setText(history.getText()+")");
            }
        });
        badd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try{
                    v1= Double.parseDouble(answer.getText()+"");
                    add=true;
                    answer.setHint(answer.getText()+"+");
                    answer.setText(null);
                    history.setText(history.getText()+"+");
                }catch(Exception e){}
            }
        });
        bsub.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try{
                    v1= Double.parseDouble(answer.getText()+"");
                    sub=true;
                    answer.setHint(answer.getText()+"-");
                    answer.setText(null);
                    history.setText(history.getText()+"-");
                }catch(Exception e){
                    answer.setText("-");
                    history.setText("-");
                }
            }
        });
        bdiv.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try{
                    v1= Double.parseDouble(answer.getText()+"");
                    div=true;
                    answer.setHint(answer.getText()+"/");
                    answer.setText(null);
                    history.setText(history.getText()+"/");
                }catch(Exception e){}
            }
        });
        bmul.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try{
                    v1= Double.parseDouble(answer.getText()+"");
                    mul=true;
                    answer.setHint(answer.getText()+"*");
                    answer.setText(null);
                    history.setText(history.getText()+"*");
                }catch(Exception e){}
            }
        });
        beql.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //object initialization
                calculations cal=new calculations();

                v2= Double.parseDouble(answer.getText()+"");
                if(div==true) {
                    answer.setText(cal.divValues(v1,v2)+ "");
                    div = false;
                }
                if(mul==true) {
                    answer.setText(cal.mulValues(v1,v2)+"");
                    mul = false;
                }
                if(add==true) {
                    answer.setText(cal.addValues(v1,v2)+"");
                    add = false;
                }
                if(sub==true) {
                    answer.setText(cal.subValues(v1,v2)+ "");
                    sub = false;
                }
                history.setHint("Result");
                history.setText(answer.getText());
                history.setVisibility(TextView.INVISIBLE);
                answer.setVisibility(TextView.VISIBLE);
            }
        });
    }
}
