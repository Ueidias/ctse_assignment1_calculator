package com.example.calassignment;

public class calculations {
    //addition
    public double addValues(double val1,double val2){
        return val1+val2;
    }
    //subtraction
    public double subValues(double val1,double val2){
        return val1-val2;
    }
    //multiplication
    public double mulValues(double val1,double val2){
        return val1*val2;
    }
    //division
    public double divValues(double val1,double val2){
        return val1/val2;
    }
}
